import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import Loading from "./components/layout/Loading";
import "./i18n";
import Router from "./Router";
import "./styles/app.scss";

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<Loading />}>
      <Router />
    </Suspense>
  </React.StrictMode>,
  document.getElementById("root")
);
