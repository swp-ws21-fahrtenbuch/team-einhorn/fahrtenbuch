import { Button, Col, Container, Image, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import logo from "../../assets/logo_unisport.png";

function Home() {
  const { t } = useTranslation();
  return (
    <div style={{height: "100%", width: "100%", position: "absolute", textAlign: "center"}}>
      <div className="vertical-center">
      <div> 
            <Image src={logo} fluid className="mb-5"/>
            <div style={{marginTop: "35%"}}>

              <Link to="/checkin">
                <Button
                  variant="primary"
                  className="w-100 mb-2 text-uppercase text-secondary border"
                >
                  {t("home.buttonBookBoat")}
                </Button>
              </Link>
              <br />
              <Link to="/login">
                <Button
                  variant="secondary"
                  className="w-100 mb-2 text-uppercase border"
                >
                  {t("home.buttonStaffLogin")}
                </Button>
              </Link>
            </div>
      </div>
      </div>
    </div>
  );
}

export default Home;
