import { useEffect, useState,useRef } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { Controller, useForm, useFieldArray } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import validator from "validator";
import Divider from "../../components/layout/Divider";
import Modal from "../../components/Modal";
import { faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getSportsFromApi } from "../../api/sport";
import { getBoatsOverview } from "../../api/boat";
import { checkIn } from "../../api/checkIn";
import { Sport, OverviewBoat, CheckIn } from "../../types";

function Book() {
  let mounted = useRef(true);
  const [sports, setSports] = useState<Sport[]>([]);
  const [boats, setBoats] = useState<OverviewBoat[]>([]);
  async function getSports() {
    let sports = await getSportsFromApi();
    if (mounted){
      setSports(sports);
    }
  }
  async function getBoats() {
    const boats = ((await getBoatsOverview()).filter(x => x.status === 0).sort((a, b) => a.name.localeCompare(b.name)));
    if (mounted){
      setBoats(boats);
    }
  }

  useEffect(() => {
    getSports();
    getBoats();
    return ()=>{
      mounted.current = false;
    }
  }, []);

  let date = new Date();
  const zeroPad = (num: number, places: number) =>
    String(num).padStart(places, "0");
  const navigate = useNavigate();
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch
  } = useForm<CheckIn>({
    mode: "onBlur",
    reValidateMode: 'onChange',
    defaultValues: {
      sport: "",
      boatName: "",
      startTime:
        zeroPad(date.getHours(), 2) + ":" + zeroPad(date.getMinutes(), 2),
      estimatedEndTime:
        zeroPad((date.getHours() + 2) % 24, 2) + ":" + zeroPad(date.getMinutes(), 2),
      destination: "",
      responsible: "",
      persons: [],
    },
  });
  const {
    fields: persons,
    append: appendPerson,
    remove: removePerson,
  } = useFieldArray({
    control,
    name: "persons",
  });
  const { t } = useTranslation(); // i18n
  const onSubmit = (data: CheckIn) => {
    let s = async () => {
      try {
        await checkIn(data);
        navigate("/checkin/success");
      } catch (e) {
        alert("error");
      }
    }
    s()
  };
  return (
    <Container className="pt-5">
      <Row>
        <Col xs={{ span: 10, offset: 1 }}>
          <Modal>
            <h1 className="text-center">{t("bookingForm.title")}</h1>
            <p className="text-center">{t("bookingForm.subtitle")}</p>
            <Divider />
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Row>
                <Col>
                  <Controller
                    name="sport"
                    control={control}
                    render={({ field }) => (
                      <div className="mb-2 required">
                        <Form.Label>{t("bookingForm.labelSport")}</Form.Label>
                        <Form.Select aria-label="Default select example" {...field}>
                          <option></option>
                          {sports.map((x) => (
                            <option key={x.id} value={x.id}>{x.name}</option>
                          ))}
                        </Form.Select>
                      </div>
                    )}
                    rules={{
                      required: {
                        value: true,
                        message: t("common.messages.required", {
                          val: t("bookingForm.labelSport"),
                        }),
                      },
                    }}
                  />
                </Col>
                <Col>
                  <Controller
                    name="boatName"
                    control={control}
                    render={({ field }) => (
                      <div className="mb-2 required">
                        <Form.Label>{t("bookingForm.labelBoatName")}</Form.Label>
                        <Form.Select aria-label="Default select example" {...field}>
                          <option></option>
                          {boats.filter(x => x.boattype.Sports.some(x => x.id === watch("sport"))).map((x) => (
                            <option key={x.id} value={x.id}>{x.name}</option>
                          ))}
                        </Form.Select>
                      </div>
                    )}
                    rules={{
                      required: {
                        value: true,
                        message: t("common.messages.required", {
                          val: t("bookingForm.labelBoatName"),
                        }),
                      },
                    }}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Controller
                    name="startTime"
                    control={control}
                    render={({ field }) => (
                      <div className="mb-2 required">
                        <Form.Label>{t("bookingForm.labelStartTime")}</Form.Label>
                        <Form.Control type="time" {...field} />
                      </div>
                    )}
                    rules={{
                      required: {
                        value: true,
                        message: t("common.messages.required", {
                          val: t("bookingForm.labelStartTime"),
                        }),
                      },
                      pattern: {
                        value: /(((0|1)\d)|(2[0-3])):[0-5]\d/,
                        message: t("bookingForm.messages.invalidTime", {
                          val: t("bookingForm.labelStartTime"),
                        }),
                      },
                    }}
                  />
                </Col>
                <Col>
                  <Controller
                    name="estimatedEndTime"
                    control={control}
                    render={({ field }) => (
                      <div className="mb-2 required">
                        <Form.Label>
                          {t("bookingForm.labelEstimatedEndTime")}
                        </Form.Label>
                        <Form.Control type="time" {...field} />
                      </div>
                    )}
                    rules={{
                      required: {
                        value: true,
                        message: t("common.messages.required", {
                          val: t("bookingForm.labelEstimatedEndTime"),
                        }),
                      },
                      pattern: {
                        value: /(((0|1)\d)|(2[0-3])):[0-5]\d/,
                        message: t("bookingForm.messages.invalidTime", {
                          val: t("bookingForm.labelEstimatedEndTime"),
                        }),
                      },
                    }}
                  />
                </Col>
              </Row>
              <Controller
                name="destination"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <div className="mb-2 required">
                    <Form.Label>{t("bookingForm.labelDestination")}</Form.Label>
                    <Form.Control
                      type="text"
                      {...field}
                      isInvalid={!!errors.destination}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.destination?.message}
                    </Form.Control.Feedback>
                  </div>
                )}
                rules={{
                  required: {
                    value: true,
                    message: t("common.messages.required", {
                      val: t("bookingForm.labelDestination"),
                    }),
                  },
                }}
              />
              <Controller
                name="email"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <div className="mb-2 required">
                    <Form.Label>{t("bookingForm.labelEmail")}</Form.Label>
                    <Form.Control
                      type="email"
                      {...field}
                      isInvalid={!!errors.email}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.email?.message}
                    </Form.Control.Feedback>
                  </div>
                )}
                rules={{
                  required: {
                    value: true,
                    message: t("common.messages.required", {
                      val: t("bookingForm.labelEmail"),
                    }),
                  },
                  validate: (value: string) =>
                    validator.isEmail(value) ||
                    t("bookingForm.messages.invalidEmail").toString(),
                }}
              />
              <Controller
                name="responsible"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <div className="mb-2 required">
                    <Form.Label>{t("bookingForm.labelName")}</Form.Label>
                    <Form.Control
                      type="text"
                      {...field}
                      isInvalid={!!errors.responsible}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.responsible?.message}
                    </Form.Control.Feedback>
                  </div>
                )}
                rules={{
                  required: {
                    value: true,
                    message: t("common.messages.required", {
                      val: t("bookingForm.labelName"),
                    }),
                  },
                }}
              />
              <div className="d-flex justify-content-between">
                <h5>{t("bookingForm.labelAdditionalNames")}</h5>
                <Button
                  variant="secondary"
                  disabled={watch("persons").length === (boats.find(x => x.id === watch("boatName"))?.boattype?.seats || 1) - 1}
                  type="button"
                  onClick={() => appendPerson({ passenger: "" })}
                >
                  +
                </Button>
              </div>

              <ul>
                {persons.map((item: any, index: number) => (
                  <div key={item.id}>
                    <div className="d-flex my-2">
                      <Controller
                        name={`persons.${index}.passenger` as const}
                        control={control}
                        render={({ field }) => (
                          <>
                            <Form.Control
                              type="text"
                              {...field}
                              isInvalid={!!errors.email}
                            />
                            <Button
                              className="mx-3"
                              variant="danger"
                              type="button"
                              onClick={() => removePerson(index)}
                            >
                              <FontAwesomeIcon
                                icon={faTrashAlt}
                                className="text-white"
                              />
                            </Button>
                          </>
                        )}
                      />
                    </div>
                  </div>
                ))}
              </ul>
              <Button type="submit" variant="secondary" className="mt-2 w-100">
                {t("bookingForm.buttonBookBoat")}
              </Button>
            </Form>
          </Modal>
        </Col>
      </Row>
    </Container >
  );
}

export default Book;
