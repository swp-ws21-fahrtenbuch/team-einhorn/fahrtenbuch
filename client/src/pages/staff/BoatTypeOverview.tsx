import { useEffect, useState, useRef } from "react";
import { Table, Button } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { faEdit, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { faArrowDown, faArrowUp, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import StaffModal from "../../components/StaffModal";
import { Form } from "react-bootstrap";
import { Controller, useForm, useFieldArray } from "react-hook-form";
import { BoatType, Sport } from "../../types";
import {
    deleteBoatType,
    getBoatTypesFromApi,
    addBoatType,
    editBoatType,
} from "../../api/boatTypes";
import { getSportsFromApi } from "../../api/sport";
import { NavLink } from "react-router-dom";
import TableSearch from "../../components/TableSearch";


function BoatTypeOverview() {
    let mounted = useRef(true);
    const { t } = useTranslation();
    const [editElement, setEditElement] = useState<BoatType | undefined>(
        undefined
    );
    const [deleteElement, setDeleteElement] = useState<number>(-1);
    const [isAdding, setAdding] = useState<boolean>(false);
    const [boatTypes, setBoatTypes] = useState<BoatType[]>([]);
    const [sports, setSports] = useState<Sport[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [typeNameSearch, setTypeNameSearch] = useState<string>("");
    const [typeSportSearch, setTypeSportSearch] = useState<string>("");
    const [typeSeatsSearch, setTypeSeatsSearch] = useState<string>("");

    const [sortBy, setSortBy] = useState<string>("name");
    const [isAsc, setIsAsc] = useState<boolean>(false);

    function setSort(by: string) {
        if (sortBy !== by) {
            setSortBy(by);
            setIsAsc(false);
        } else {
            setIsAsc(!isAsc);
        }
    }

    const {
        control,
        setValue,
        handleSubmit,
        formState: { errors, isValid },
    } = useForm<BoatType>({
        mode: "onBlur",
        reValidateMode: 'onChange'
    });

    const {
        fields: sportField,
        append: appendSport,
        remove: removeSport,
        replace: replaceSport
    } = useFieldArray({
        control,
        name: "Sports",
    });

    async function getBoatTypes() {
        let boatTypes = await getBoatTypesFromApi()
        if (mounted) {
            setBoatTypes(boatTypes);
        }
    }
    async function getSports() {
        let sports = await getSportsFromApi();
        if (mounted) {
            setSports(sports);
        }
    }

    useEffect(() => {
        getBoatTypes();
        getSports();
        return () => {
            mounted.current = false;
        }
    }, []);

    return (
        <div className="m-1 h-100">
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>
                            <span onClick={() => { setSort("name") }}>
                                {t("boatTypeOverview.Name")}
                                {sortBy === "name" && <FontAwesomeIcon icon={isAsc ? faArrowUp : faArrowDown} />}
                            </span>
                            <TableSearch state={typeNameSearch} onChange={(data) => { setTypeNameSearch(data) }}></TableSearch>
                        </th>
                        <th>
                            <span onClick={() => { setSort("seats") }}>
                                {t("boatTypeOverview.Seats")}
                                {sortBy === "seats" && <FontAwesomeIcon icon={isAsc ? faArrowUp : faArrowDown} />}
                            </span>
                            <TableSearch state={typeSeatsSearch} onChange={(data) => { setTypeSeatsSearch(data) }}></TableSearch>
                        </th>
                        <th>
                            <span>
                                {t("boatTypeOverview.Sport")}
                            </span>
                            <TableSearch state={typeSportSearch} onChange={(data) => { setTypeSportSearch(data) }}></TableSearch>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody style={{ overflowY: "scroll", maxHeight: "100px" }}>
                    {boatTypes
                        .sort((a, b) => {
                            return (b[sortBy as keyof BoatType]?.toString().localeCompare(a[sortBy as keyof BoatType].toString() || "") || 0) * (isAsc ? 1 : -1)
                        })
                        .filter((boatType) => boatType.name.toLocaleLowerCase().includes(typeNameSearch.toLocaleLowerCase()))
                        .filter((boatType) => boatType.Sports.map(x => x.name).join(', ').toLocaleLowerCase().includes(typeSportSearch.toLocaleLowerCase()))
                        .filter((boatType) => typeSeatsSearch === "" || Number(boatType.seats)===Number(typeSeatsSearch))
                        .map((x, i) => (
                            <tr key={x.id}>
                                <td><NavLink to={"/staff/statistics/boattype/" + x.id}>{x.name}</NavLink></td>
                                <td>{x.seats}</td>
                                <td>{x.Sports.map(x => x.name).join(', ')}</td>
                                <td>
                                    <div className="d-flex">
                                        <div
                                            className="mx-2"
                                            onClick={() => {
                                                setLoading(false);
                                                setEditElement(x);
                                                setValue("id", x.id);
                                                setValue("name", x.name);
                                                setValue("seats", x.seats);
                                                replaceSport(x.Sports);
                                            }}
                                        >
                                            <FontAwesomeIcon
                                                icon={faEdit}
                                                className="text-secondary clickableIcon"
                                                size="1x"
                                            />
                                        </div>
                                        <div
                                            className="mx-2"
                                            onClick={() => {
                                                setDeleteElement(boatTypes.indexOf(x));
                                            }}
                                        >
                                            <FontAwesomeIcon
                                                icon={faTrashAlt}
                                                className="text-danger clickableIcon"
                                                size="1x"
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </Table>
            <div
                className="d-flex px-2 py-1 justify-content-end bg-white border-top"
                style={{ position: "sticky", right: "5px", bottom: "0", zIndex: 3 }}
            >
                <Button
                    onClick={() => {
                        setAdding(true);
                        setEditElement({
                            id: "",
                            name: "",
                            seats: 1,
                            Sports: [{ id: undefined as any }],
                        } as BoatType);
                        setValue("id", undefined as any);
                        setValue("name", "");
                        setValue("seats", 1);
                        replaceSport([{ id: undefined as any }]);
                    }}
                    variant="secondary"
                >
                    <FontAwesomeIcon icon={faPlus} className="text-white me-2" />
                    {t("boatTypeOverview.addBoatType")}
                </Button>
            </div>
            <StaffModal
                header={
                    isAdding
                        ? t("boatTypeOverview.addBoatType")
                        : t("boatTypeOverview.EditBoatType")
                }
                show={!!editElement}
                successText={
                    isAdding ? t("common.Add") : t("common.Edit")
                }
                disableNext={!isValid}
                loadingNext={loading}
                onHide={() => {
                    setEditElement(undefined);
                    setAdding(false);
                }}
                onSuccess={() => {
                    setLoading(true);
                    handleSubmit((data) => {
                        if (isAdding) {
                            addBoatType(data).then(async () => {
                                await getBoatTypes();
                                setEditElement(undefined);
                                setAdding(false);
                                setLoading(false);
                            }).catch(() => {
                                setLoading(false);
                            });
                        } else {
                            editBoatType(data).then(async () => {
                                await getBoatTypes();
                                setEditElement(undefined);
                                setAdding(false);
                                setLoading(false);
                            }).catch(() => {
                                setLoading(false);
                            });
                        }
                    })();
                }}
            >
                <Form>
                    {!isAdding && <Controller
                        name="id"
                        control={control}
                        defaultValue={editElement?.id}
                        render={({ field }) => (
                            <div className="mb-2">
                                <Form.Label>{t("boatTypeOverview.Id")}</Form.Label>
                                <Form.Control disabled type="text" {...field} />
                            </div>
                        )}
                    />}
                    <Controller
                        name="name"
                        control={control}
                        defaultValue={editElement?.name}
                        render={({ field }) => (
                            <div className="mb-2">
                                <Form.Label>{t("boatTypeOverview.Name")}</Form.Label>
                                <Form.Control
                                    type="text"
                                    {...field}
                                    isInvalid={!!errors.name}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errors.name?.message}
                                </Form.Control.Feedback>
                            </div>
                        )}
                        rules={{
                            required: {
                                value: true,
                                message: t("common.messages.required", {
                                    val: t("boatTypeOverview.Name"),
                                }),
                            },
                        }}
                    />
                    <Controller
                        name="seats"
                        control={control}
                        defaultValue={editElement?.seats}
                        render={({ field }) => (
                            <div className="mb-2">
                                <Form.Label>{t("boatTypeOverview.Seats")}</Form.Label>
                                <Form.Control
                                    type="number"
                                    min="1"
                                    {...field}
                                    isInvalid={!!errors.seats}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errors.seats?.message}
                                </Form.Control.Feedback>
                            </div>
                        )}
                        rules={{
                            required: {
                                value: true,
                                message: t("common.messages.required", {
                                    val: t("boatTypeOverview.Seats"),
                                }),
                            },
                        }}
                    />
                    <div className="d-flex justify-content-between">
                        <h5>{t("boatTypeOverview.Sports")}</h5>
                        <Button
                            variant="secondary"
                            type="button"
                            onClick={() => appendSport({ id: undefined })}
                        >
                            +
                        </Button>
                    </div>

                    <ul>
                        {sportField.map((item: any, index: number) => (
                            <div key={item.id}>
                                <div className="d-flex my-2">
                                    <Controller
                                        name={`Sports.${index}.id` as const}
                                        control={control}
                                        render={({ field }) => (
                                            <Form.Select {...field} isInvalid={!!errors.Sports?.[index]?.id}>
                                                <option></option>
                                                {sports.map((x) => (
                                                    <option key={x.id} value={x.id}>
                                                        {x.name}
                                                    </option>
                                                ))}
                                            </Form.Select>
                                        )}
                                        rules={{
                                            required: {
                                                value: true,
                                                message: t("common.messages.required", {
                                                    val: t("boatTypeOverview.Sport"),
                                                }),
                                            },
                                        }}
                                    />
                                    <Button
                                        className="mx-3"
                                        variant="danger"
                                        type="button"
                                        onClick={() => removeSport(index)}
                                    >
                                        <FontAwesomeIcon
                                            icon={faTrashAlt}
                                            className="text-white"
                                        />
                                    </Button>
                                </div>
                            </div>
                        ))}
                    </ul>
                </Form>
            </StaffModal>
            <StaffModal
                header={t("boatTypeOverview.DeleteBoatType")}
                hideColor="secondary"
                successText={t("common.Delete")}
                successColor="danger"
                show={deleteElement !== -1}
                loadingNext={loading}
                onHide={() => {
                    setDeleteElement(-1);
                }}
                onSuccess={async () => {
                    setLoading(true);
                    await deleteBoatType({ id: boatTypes[deleteElement].id });
                    await getBoatTypes();
                    setDeleteElement(-1);
                    setLoading(false);
                }}
            >
                <span>
                    {t("boatTypeOverview.messages.DeleteText", {
                        val: boatTypes[deleteElement]?.name,
                    })}
                </span>
            </StaffModal>
        </div>
    );
}

export default BoatTypeOverview;
