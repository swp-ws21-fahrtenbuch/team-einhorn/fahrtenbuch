import { useEffect, useState, useRef, useCallback } from "react";
import { Table } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CheckIn } from "../../types";
import { getAllCheckIns, markComment } from "../../api/checkIn";
import { faArrowDown, faArrowUp, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import TableSearch from "../../components/TableSearch";
import TableFilter from "../../components/TableFilter";
import TableDateFilter from "../../components/TableDateFilter";

function Comments() {
  let mounted = useRef(true);
  const { t } = useTranslation();
  const [checkIns, setcheckIns] = useState<CheckIn[]>([]);
  const [commentSearch, setCommentSearch] = useState<string>("");

  const [dateStartSearch, setDateStartSearch] = useState<string>("");
  const [dateEndSearch, setDateEndSearch] = useState<string>("");
  //Hack for refreshing when marking done/undone
  const [show, setShow] = useState<string[]>(["notdone"]);
  const [toggleElement, setToggleElement] = useState<CheckIn | null>(null);

  const [sortBy, setSortBy] = useState<string>("date");
  const [isAsc, setIsAsc] = useState<boolean>(true);

  async function getcheckIns() {
    const checkIns = await getAllCheckIns();
    if (mounted) {
      setcheckIns(checkIns);
    }
  }

  const toggle = useCallback(async () => {
    if (toggleElement === null) return;
    await markComment(!toggleElement.noteDone, toggleElement.id);
    setToggleElement(null);
  }, [toggleElement]);

  function setSort(by: string) {
    if (sortBy !== by) {
      setSortBy(by);
      setIsAsc(false);
    } else {
      setIsAsc(!isAsc);
    }
  }

  useEffect(() => {
    getcheckIns();
    toggle();
    return () => {
      mounted.current = false;
    }
  }, [toggle]);

  return (
    <div className="m-1 h-100">
      <Table responsive striped bordered hover>
        <thead>
          <tr>
            <th>
              <span onClick={() => { setSort("note") }}>
                {t("comments.Comment")}
                {sortBy === "note" && <FontAwesomeIcon icon={isAsc ? faArrowUp : faArrowDown} />}
              </span>
              <TableSearch state={commentSearch} onChange={(data) => { setCommentSearch(data) }}></TableSearch>
            </th>
            <th>
              <span onClick={() => { setSort("date") }}>
                {t("comments.Date")}
                {sortBy === "date" && <FontAwesomeIcon icon={isAsc ? faArrowUp : faArrowDown} />}
              </span>
              <TableDateFilter stateFrom={dateStartSearch} stateTo={dateEndSearch} onChangeTo={(data) => {
                setDateEndSearch(data);
              }} onChangeFrom={(data) => {
                setDateStartSearch(data);
              }}></TableDateFilter>
            </th>
            <th>
              <TableFilter options={[
                { label: t("comments.done"), value: "done" },
                { label: t("comments.notDone"), value: "notdone" },
              ]} state={show} onChange={(data) => { setShow(data) }}></TableFilter>
            </th>
          </tr>
        </thead>
        <tbody>
          {
            checkIns
              .filter((checkIn) => checkIn.returned && checkIn.note)
              .filter((checkIn) => {
                return (show.includes("done") && checkIn.noteDone) || (show.includes("notdone") && !checkIn.noteDone)
              })
              .filter((checkIn) => checkIn.note.toLocaleLowerCase().includes(commentSearch.toLocaleLowerCase()))
              .filter((checkIn) => {
                if (dateEndSearch !== "" && dateStartSearch !== "") {
                  return Date.parse(checkIn.date) >= Date.parse(dateStartSearch) && Date.parse(checkIn.date) <= Date.parse(dateEndSearch);
                }
                else if (dateStartSearch !== "" && dateEndSearch === "") {
                  return Date.parse(checkIn.date) === Date.parse(dateStartSearch);
                }
                //dateStartSearch should never be Empty when dateEndSearch is not empty
                else {
                  return true;
                }
              })
              .sort((a, b) => {
                if (sortBy === "date") {
                  return Date.parse(b.date) - Date.parse(a.date) * (isAsc ? 1 : -1);
                } else {
                  return (b[sortBy as keyof CheckIn]?.toString().localeCompare(a[sortBy as keyof CheckIn]?.toString() || "") || 0) * (isAsc ? 1 : -1)
                }
              })
              .map((x, i) => (
                <tr key={x.id}>
                  <td>{x.note}</td>
                  <td>
                    {new Date(x.date).toLocaleDateString()}
                  </td>
                  <td>
                    <div className="d-flex">
                      <div className="mx-2">
                        <FontAwesomeIcon
                          icon={x.noteDone ? faCheckCircle : faCheckCircle}
                          className={`${x.noteDone ? "text-success" : "text-muted"} clickableIcon`}
                          size="1x"
                          title={x.noteDone ? t("comments.MarkUndone") : t("comments.MarkDone")}
                          onClick={() => {
                            setToggleElement(x);
                          }}
                        />
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
        </tbody>
      </Table>
    </div>
  );
}

export default Comments;
