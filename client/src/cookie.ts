export function setCheckInCookie(id: string) {
    document.cookie = `checkinId=${id}; max-age=${24 * 60 * 60 * 1000}; path=/`
}
export function getCheckInCookie(): string | undefined {
    let value = ('; ' + document.cookie).split(`; checkinId=`).pop()?.split(';')[0];
    return value
}
export function eraseCheckInCookie(): void {
    document.cookie = 'checkinId=; Path=/; max-age=0;';
}