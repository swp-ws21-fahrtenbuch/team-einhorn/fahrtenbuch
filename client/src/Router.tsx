import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import StaffLayout from "./components/StaffLayout";
import UserLayout from "./components/UserLayout";
import BoatManager from "./pages/staff/BoatManager";
import BoatOverview from "./pages/staff/BoatOverview";
import BoatTypeOverview from "./pages/staff/BoatTypeOverview";
import BookingForm from "./pages/public/BookingForm";
import BookingSignOut from "./pages/public/BookingSignOut";
import BookingSignOutSuccess from "./pages/public/BookingSignOutSuccess";
import BookingSuccessful from "./pages/public/BookingSuccessful";
import Home from "./pages/public/Home";
import StaffLogin from "./pages/staff/StaffLogin";
import StatisticOverview from "./pages/staff/Statistics";
import Sports from "./pages/staff/Sports";
import Accounts from "./pages/staff/Accounts";
import { useEffect, useState } from "react";
import { getCurrentUser } from "./api/user";
import { getCurrentCheckIn } from "./api/checkIn";
import Comments from "./pages/staff/Comments";
import SingleStatisticSport from "./pages/staff/SingleStatisticSport";
import SingleStatisticBoatType from "./pages/staff/SingleStatisticBoattype";

function Router() {
  const [isStaff, setIsStaff] = useState(false);
  const [isCoord, setIsCoord] = useState(false);
  const [checkInCookie, setCheckInCookie] = useState<string | null>(null);
  const [isval, setIsval] = useState(false);

  async function validateCurrentUser() {
    try {
      const response = await getCurrentUser();
      setIsStaff(response.status === 200);
      setIsval(true);
      if (response.status === 200) {
        const json = await response.json();
        setIsCoord(json.result.role === "coordinator");
      }
    } catch (e) { }
  }

  async function checkForCheckInCookie() {
    try {
      const response = await getCurrentCheckIn();
      if (response.status === 200) {
        const json = await response.json();
        setCheckInCookie(json.result);
      }
    } catch (e) { }
  }

  useEffect(() => {
    validateCurrentUser();
    checkForCheckInCookie();
  });

  return (
    <HelmetProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<UserLayout />}>
            {/* Home page (user) */}
            <Route
              index
              element={
                checkInCookie ? (
                  <Navigate to={"/checkout/" + checkInCookie} />
                ) : (
                  <Home />
                )
              }
            />
            {/* Book a boat (user) */}
            <Route path="checkin">
              <Route index element={<BookingForm />} />
              {/* After booking (user) */}
              <Route path="success" element={<BookingSuccessful />} />
            </Route>
            <Route path="login">
              <Route
                index
                element={
                  !isStaff ? (
                    <StaffLogin
                      onChange={async () => {
                        await validateCurrentUser();
                      }}
                    />
                  ) : (
                    <Navigate to="/staff" />
                  )
                }
              />
            </Route>
            {/* Sign out (user) */}
            <Route path="checkout">
              <Route
                path=""
                element={
                  checkInCookie ? (
                    <Navigate to={"/checkout/" + checkInCookie} />
                  ) : (
                    <Home />
                  )
                }
              />
              <Route path=":id" element={<BookingSignOut />} />
              {/* After Sign out (user) */}
              <Route path="success" element={<BookingSignOutSuccess />} />
            </Route>
          </Route>
          <Route
            path="staff"
            element={
              isStaff ? (
                <StaffLayout
                  isCoord={isCoord}
                  onChange={async () => {
                    await validateCurrentUser();
                  }}
                />
              ) : isval ? (
                <Navigate to="/login" />
              ) : (
                <></>
              )
            }
          >
            <Route path="" element={<Navigate to="/staff/overview" />} />
            <Route path="overview" element={<BoatOverview />} />
            <Route path="boattypes" element={<BoatTypeOverview />} />
            <Route path="sports" element={<Sports />} />
            <Route path="boats" element={<BoatManager />} />
            <Route path="statistics" >
              <Route path="" element={<StatisticOverview />}> </Route>
              <Route path="sport">
                <Route path=":id" element={<SingleStatisticSport />} />
              </Route>
              <Route path="boattype">
                <Route path=":id" element={<SingleStatisticBoatType />} />
              </Route>
            </Route>
            <Route path="comments" element={<Comments />} />
            <Route
              path="accounts"
              element={isCoord ? <Accounts /> : <Navigate to="/staff" />}
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}

export default Router;
