export interface Sport {
  id: string;
  name: string;
  color: string;
}
export interface Boat {
  id: string;
  name: string;
  status: number;
  boattype: string;
}
export interface OverviewBoat {
  id: string;
  name: string;
  status: number;
  boattype: BoatType;
  currentCheckIn?: CheckIn;
}
export interface BoatType {
  id: string;
  name: string;
  seats: number;
  Sports: Sport[];
}
export interface Account {
  id: string;
  first_name: string;
  last_name: string;
  password?: string;
  role: "coordinator" | "boatManager";
  email: string;
}
export interface ID {
  id: string;
}
export interface CheckIn {
  id: string;
  sport: string;
  boatName: string;
  startTime: string;
  estimatedEndTime: string;
  destination: string;
  email: string;
  persons: { passenger: string }[];
  responsible: string;
  returned: boolean;
  note: string;
  noteDone: boolean;
  date: string;
}