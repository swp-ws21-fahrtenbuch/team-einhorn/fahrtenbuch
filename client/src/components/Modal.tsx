import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../assets/logo_unisport.png";
import Divider from "./layout/Divider";

function Modal(props: any) {
  return (
    <div className="my-3 p-3 bg-white bg-opacity-100 rounded-3">
      <div className="text-end">
        <Link to="/">
          <FontAwesomeIcon icon={faTimesCircle} color="rgba(0, 0, 0, 0.3)" />
        </Link>
      </div>
      {props.children}
      <div className="text-center mt-3">
        <Divider />
        <Image src={logo} className="w-25" />
      </div>
    </div>
  );
}
export default Modal;
