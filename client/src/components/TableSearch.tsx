import {  useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Form } from "react-bootstrap";


function TableSearch(props: { state: string, onChange: (val: string)=>void }) {
    const [isOpen, setOpen] = useState<boolean>(false);

    return (
        <div className={"tableSearch " + (props.state || isOpen ? "active" : "")}>
            <FontAwesomeIcon onClick={(event) => { setOpen(!isOpen); event.stopPropagation();}} icon={faSearch} className={"searchIcon"} inverse={!!props.state || isOpen}></FontAwesomeIcon>
            {isOpen &&
                <div className="popover">
                    <Form.Control
                        type="text" defaultValue={props.state}
                        className="form-control-sm"
                        placeholder="Search ..."
                        aria-label="Search"
                        onChange={(event) => {
                            props.onChange(event.target.value);
                        }}
                    />
                </div>
            }
        </div>
    );
}

export default TableSearch;
