import { useTranslation } from "react-i18next";
import { Outlet } from "react-router";
import { useLocation } from "react-router-dom";
import bg from "../assets/bg_mobile.png";
import { Helmet } from "react-helmet-async";
import LanguageSelector from "../components/LanguageSelector";


function UserLayout() {
  const { t } = useTranslation();
  const { pathname } = useLocation();

  return (
    <div>
      <Helmet>
        <title>{pathname.includes('checkout') ? t('routes./checkout') : t(`routes.${pathname}`)}</title>
      </Helmet>
      <div
        style={{
          position: "fixed",
          height: "100vh",
          width: "100vw",
          backgroundImage: `url(${bg})`,
          backgroundSize: "cover",
          backgroundPosition: "center center",
          zIndex: -1,
        }}
      />
      <div className="language-div">
        <div style={{ float: "right", paddingRight: "1%", paddingTop: "1%", position: "relative" }}>
          <LanguageSelector ></LanguageSelector>
        </div>
      </div>
      
      <Outlet />
    </div>
  );
}

export default UserLayout;
