function ToggleSwitch(props: {checked: boolean, changeFunc: (checked: boolean) => void}) {
	return (
		<div className="toggle-switch">
			<input
				type="checkbox"
				className="toggle-switch-checkbox"
				name="toggleSwitch"
				id="toggleSwitch"
				checked={props.checked}
				onChange={e => props.changeFunc(e.target.checked)}
			/>
			<label className="toggle-switch-label" htmlFor="toggleSwitch">
          		<span className="toggle-switch-inner" 
				  data-on="DE" data-off="EN"/>
          		<span className="toggle-switch-switch"/>
        	</label>
		</div>
	)
}

export default ToggleSwitch;