import { Spinner } from "react-bootstrap";

function Loading() {
  return (
    <div className="vh-100 vw-100 d-flex justify-content-center align-items-center">
      <Spinner animation="border" />
    </div>
  );
}

export default Loading;
