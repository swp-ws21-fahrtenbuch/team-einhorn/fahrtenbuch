import { BoatType, ID } from "../types"
export async function getBoatTypesFromApi(): Promise<BoatType[]> {
  const response = await fetch("/api/boattype", {
    method: "GET",
  });
  const json = await response.json();
  return json.result;
}

export async function addBoatType({
  name,
  seats,
  Sports,
}: Omit<BoatType, "id">) {
  const response = await fetch("/api/boattype", {
    method: "POST",
    body: JSON.stringify({
      name,
      seats,
      sports: Sports,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const json = await response.json();
  return json;
}

export async function editBoatType({
  id,
  name,
  seats,
  Sports,
}: BoatType) {
  const response = await fetch(`/api/boattype/${id}`, {
    method: "PATCH",
    body: JSON.stringify({
      name,
      seats,
      sports: Sports,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const json = await response.json();
  return json;
}

export async function deleteBoatType({ id }: ID) {
  const response = await fetch(`/api/boattype/${id}`, {
    method: "DELETE",
  });
  const json = await response.json();
  return json;
}
