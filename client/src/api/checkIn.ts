import { CheckIn } from "../types";

export async function markComment(done: boolean, id: string) {
  const apiString = done ? "noteDone" : "noteNotDone";
  const response = await fetch(`/api/checkin/${apiString}/${id}`, {
    method: "POST",
  });
  return response.status === 200;
}

export async function getCurrentCheckIn() {
  const response = await fetch("/api/checkin", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response;
}

export async function getCheckInByBoatId(boatId:string) {
  const response = await fetch(`/api/checkin/boat/${boatId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  const json = await response.json();
  return json;
}

export async function checkIn({
  sport,
  boatName,
  startTime,
  estimatedEndTime,
  destination,
  email,
  persons,
  responsible,
}: CheckIn) {
  let start = new Date();
  start.setHours(
    Number(startTime.split(":")[0]),
    Number(startTime.split(":")[1]),
    0,
    0
  );
  let end = new Date();
  end.setHours(
    Number(estimatedEndTime.split(":")[0]),
    Number(estimatedEndTime.split(":")[1]),
    0,
    0
  );

  const response = await fetch("/api/checkin", {
    method: "POST",
    body: JSON.stringify({
      sport,
      boatName,
      startTime: start,
      estimatedEndTime: end,
      destination,
      email,
      persons: persons.map((x) => x.passenger),
      responsible,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  await response.json();
  return response.ok;
}

export async function checkOut({
  id,
  note,
  bookingType,
}: {
  id: string;
  note: string;
  bookingType: string;
}) {
  const response = await fetch(`/api/checkout/${id}`, {
    method: "POST",
    body: JSON.stringify({
      bookingType: bookingType,
      note,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.ok;
}

export async function getAllCheckIns() {
  const response = await fetch("/api/checkins", {
    method: "GET",
  });
  const json = await response.json();
  return json.result;
}
