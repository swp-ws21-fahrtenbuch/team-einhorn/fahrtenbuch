
# Deployment

  

## Building an release image

  

Use your credentials to login:

```shell

docker login git.imp.fu-berlin.de:5000

```

  

build image locally

```shell
docker compose -f docker-compose.prod.yaml build
```

  

Push to registry

```
docker tag fahrtenbuch-server-image:latest git.imp.fu-berlin.de:5000/swp-ws21-fahrtenbuch/team-einhorn/fahrtenbuch && docker push git.imp.fu-berlin.de:5000/swp-ws21-fahrtenbuch/team-einhorn/fahrtenbuch
```

  

## Deploy on a Production server

There should be a file in `httpdocs/docker-compose.yaml` similar to the `docker-compose.prod.yaml` wich pulls the image:
```yaml
#httpdocs/docker-compose.yaml
version: "3.8"
services:
  postgres:
    image: postgres
    container_name: fahrtenbuch-postgres
    environment:
      POSTGRES_USER: ${POSTGRES_USER:-postgres}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-postgres}
      POSTGRES_DB: ${DB_NAME:-postgres}
      PGDATA: /data/postgres
    volumes:
      - postgres:/data/postgres
    networks:
      - app-network
    restart: unless-stopped

  server:
    image: git.imp.fu-berlin.de:5000/swp-ws21-fahrtenbuch/team-einhorn/fahrtenbuch:latest
    container_name: fahrtenbuch-server
    command: npm run start:prod

    ports:
      - "4000:4000"
    depends_on:
      - postgres
    env_file:
      - ./.env
    environment:
      - NODE_ENV=production
      - DB_HOST=postgres
    networks:
      - app-network
    restart: unless-stopped

networks:
  app-network:
    driver: bridge

volumes:
  postgres:
  pgadmin:
  web-root:
    driver: local
```

Also an `.env` file should be present in the form of `.env.example`



To pull image and run you first need su and then run the compose:
 (this image is **not** running after closing ssh but you could start the container afterwards from Plesk ... )
```
su
cd httpdocs && docker-compose up
```
