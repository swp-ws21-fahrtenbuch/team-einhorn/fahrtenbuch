# Localization

When creating the front end you want to make sure that the user can read text in their prefered language (if defined). We use the i18n Standard for this. Specifically we use i18next which reads the prefered user language from the browser and falls back to english if that language file does not exist.

## Adding a new Language

If you want to add a new language you have to create a new folder under `client/public/locales` with any [language code](https://www.w3.org/International/O-charset-lang.html) as the name. Then you add a `translation.json` file that has all the same keys as the already existing `translation.json` files. Then in the values you can add your tanslated text

> Tip: Make sure you don't miss any keys, the pipeline will notice

## Structure of the translation.json file

Since it's a json File you shoul follow json syntax. The translation file is basically just a large object with keys that define subobject. Any key in the parent subject corresponds to a React Component, where the text will be displayed (so make sure to use the same name as the component when adding keys). The subobjects then contain all the unique keys for that component where the corresponding value will be the display text for that language. Also make sure that keys describe what type of Element they are (e.g. `buttonButtoName` for a button) so other people know what this text is supposed to be.

So a file that looks like this

```json
{
    "home": {
        "buttonBookBoat": "Book a Boat",
        "buttonStaffLogin": "Staff Login"
    },
    "book": {
        "title": "Book a Boat!",
        "subtitle": "Please enter the information for your booking",
        "labelBoatName": "Boat name",
        "labelStartTime": "Start time",
        ...
    }
}
```

Would contain the english translation for the home and book site

## Adding translations into your react code

To use React with the i18n you should first import the `useTranslation` hook and then use it in your component (`import { useTranslation } from "react-i18next";`). Then at the beginning of your functional component you hook up the `t` and `i18n` functions with `const { t, i18n } = useTranslation();`. After that you can simply use `{t("pageName.keyName")}` anywhere you like and let i18next take care of the rest.

> Tip: remeber to use objectName.keyName to access the value for a specific page
