# Testing

## Running Tests  

Running tests is as simple as going either into the `client` or `server` directory and running `npm test`. Jest will then automatically take all the `*.test.ts` files and run them
> Tip: Before running any test against the client or server, make sure you have the server and/or client running

It's very important to note that the Pipeline on Gitlab will execute all the tests, so expect failed pipelines if you push broken code.

## Writing tests

To create your own tests, just create a new file in the `test` directory of the part you want to test. You could also change an existing file or even create a subdirectory with files. It's important to note that any and all tests files should end with `.test.ts` otherwise Jest will not run them.

### Structuring tests

Since testing is done with [jest](https://www.npmjs.com/package/jest) we can follow a specific structure like this:

```javascript
describe('Test', () => {
  it('should do something', (done) => {
    // Do something
  });
  it('should do something else', (done) => {
    // Do something else
  });
});
```

The `describe` function can be used to group specific tests into one Test Suite, so that you can have similar tests be run together. In this example the Suite would be calles "Test". `it` is used to define a single test. The first argument is simply the test name, the second parameter will be the actual test thats run. Whatever you decide to define here, will be executed. The `done` parameter is optional and can be used if you expect callback.
> Tip: make sure to create tests that maybe only test a single function or a small piece of code and be sure to use descriptive test names, since tests can already serve as documentation.

## API Testing

Using [supertest](https://www.npmjs.com/package/supertest) and the structure above, we can easily create API tests. We only have to modify our structure a little bit.

```javascript
import request from 'supertest';

describe('Test', () => {
  it('should do something', (done) => {
    //The URL should be always this with our project settings
    request("http://localhost:4000")
      .get('/api/v1/test')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  });
});
```

This example sends a GET request to the `/api/v1/test` endpoint and expects to receive a 200 status code and a JSON response.

### GET Requests

GET Requests are already demonstrated in the example above.

>Tip: if you need to send a GET request with authentication, you can use the `.auth(username, password)` function to do so.

### POST Requests

As you can guess the requests are the same as GET requests, but with the exception of the `.post()` method.

```javascript
import request from 'supertest';

describe('Test', () => {
  it('should do something', (done) => {
    //The URL should be always this with our project settings
    request("http://localhost:4000")
      .post('/api/v1/users')
      .send({name:'john'})
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  });
});
```

### Expectations

If you expect your request to yield a response with specific content, you can use the `.expect()` function. For example if you post a Username and expect to get a fixed ID, you can do this:

```javascript
describe('POST /user', () => {
  it('user.name should be an case-insensitive match for "john"', (done) => {
    request(app)
      .post('/user')
      .send('name=john') // x-www-form-urlencoded upload
      .set('Accept', 'application/json')
      .expect(function(res) {
        res.body.id = 'some fixed id';
        res.body.name = res.body.name.toLowerCase();
      })
      .expect(200, {
        id: 'some fixed id',
        name: 'john'
      }, done);
  });
});
```

You can do a lot more with supertest, basically everything you can to with superagent, so refer to the [supertest documentation](https://www.npmjs.com/package/supertest) and the [superagent documentation](https://www.npmjs.com/package/superagent)for more information.

## React Testing

Testing React/the Frontend works similar as API testing.

Refer to the official [Jest Testing Documentation](https://jestjs.io/docs/tutorial-react) to find out how to do it
