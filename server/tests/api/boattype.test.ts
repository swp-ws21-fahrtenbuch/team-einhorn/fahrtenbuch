import {
	randomUUID
} from "crypto";
import request, {
	SuperAgentTest
} from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Boat Type Route", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		server = request.agent("http://localhost:4000");
	});
	describe("Invalid JWT Requests", () => {
		it("should 401 (Unauthorized) when GETting all Boat Type", (done) => {
			server
				.get("/api/boattype/")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when posting a Boat Type", (done) => {
			server
				.post("/api/boattype/")
				.send({
					name: "Boat",
					seats: 4,
					sports: [{id: randomUUID()}]
				})
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when patching a Boat Type", (done) => {
			server
				.patch("/api/boattype/does_not_matter")
				.send({
					name: "Boat",
					seats: 4,
					sports: [{id: randomUUID()}]
				})
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when DELETEing a Boat Type", (done) => {
			server
				.delete("/api/boattype/does_not_matter")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
		it("should 401 (Unauthorized) when GETTing a Boat Type", (done) => {
			server
				.get("/api/boattype/does_not_matter")
				.expect(401)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
		});
	});
	describe("Valid JWT Requests", () => {
		let boatTypeId: string;
		let nonExistBoatTypeId = randomUUID();
		let sportId: string;
		beforeAll(async () => {
			await server
				.post("/api/login/")
				.send({
					email: email,
					password: password
				});
			// We need a sport for the boattype
			const sport = await server
				.post("/api/sport/")
				.send({
					name: "Test Sport"
				});
			sportId = sport.body.result.id;
		});
		describe("Error Requests", () => {
			it("should 404 when GETting Boat Type from non existing boat", (done) =>{
				server
				.get(`/api/boattype/${nonExistBoatTypeId}`)
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('boatIdNotFound');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 404 when PATCHing non existing Boat Type", (done) => {
				server
				.patch(`/api/boattype/${nonExistBoatTypeId}`)
				.send({name: "Big Boat", seats: 4, sports: [{id: randomUUID()}]})
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('boatTypeIdNotFound');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 404 when DELETEing non existing Boat Type", (done) => {
				server
				.delete(`/api/boattype/${nonExistBoatTypeId}`)
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('boatTypeIdNotFound');
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when POSTing without name", (done) => {
				server
				.post("/api/boattype/")
				.send({seats: 4, sports: [{id: sportId}]})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toEqual([{
						msg: 'Invalid value',
						param: 'name',
						location: 'body'
					}]);
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when POSTing without seats", (done) => {
				server
				.post("/api/boattype/")
				.send({name: "Boat Type", sports: [{id: sportId}]})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'seats',
						location: 'body'
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when POSTing with negative seats", (done) => {
				server
				.post("/api/boattype/")
				.send({name: "Boat Type", seats: -1, sports: [{id: sportId}]})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'seats',
						location: 'body',
						value: -1
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when POSTing without sports", (done) => {
				server
				.post("/api/boattype/")
				.send({name: "Boat Type", seats: 4})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'sports',
						location: 'body'
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when PATCHing with empty name", (done) => {
				server
				.post("/api/boattype/")
				.send({name: "",})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'name',
						location: 'body',
						value: ''
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when PATCHing with non numeric seats", (done) => {
				server
				.post("/api/boattype/")
				.send({seats: '',})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'seats',
						location: 'body',
						value: ''
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when PATCHing with empty sports", (done) => {
				server
				.post("/api/boattype/")
				.send({sports: [],})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'sports',
						location: 'body',
						value: []
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
			it("should 400 (Bad Request) when PATCHing with non array sports", (done) => {
				server
				.post("/api/boattype/")
				.send({sports: "",})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toContainEqual({
						msg: 'Invalid value',
						param: 'sports',
						location: 'body',
						value: ''
					});
				})
				.end((err) => {
					if (err) return done(err);
					return done();
				});
			});
		});
		it("should 201 (Created) when posting boat", (done) => {
			server
			.post("/api/boattype/")
			.send({name: "Boat Type", seats: 4, sports: [{id: sportId}]})
			.expect(201)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.id).toBeDefined();
				expect(res.body.result.name).toBe("Boat Type");
				expect(res.body.result.seats).toBe(4);
				boatTypeId = res.body.result.id;
				while(nonExistBoatTypeId === boatTypeId) nonExistBoatTypeId = randomUUID();
			})
			.end((err) => {
				if (err) return done(err);
				return done();
			});
		}); 
		it("should 200 (OK) with result when getting all Boat Types", (done) => {
			server
			.get("/api/boattype/")
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.length).toBeGreaterThan(0);
			})
			.end((err) => {
				if (err) return done(err);
				return done();
			});
		});
		/*
		Apparently I am not going to test that ¯\_(ツ)_/¯
		it("should 200 (OK) with result when getting one Boat Type", (done) => {
			server
			.get(`/api/boattype/${boatTypeId}`)
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.length).toBeGreaterThan(0);
			})
			.end((err) => {
				if (err) return done(err);
				return done();
			});
		});*/
		it("should 200 (OK) with result when patching Boat Type", (done) => {
			server
			.patch(`/api/boattype/${boatTypeId}`)
			.send({name: "Weird Boat Type"})
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.name).toEqual("Weird Boat Type");
			})
			.end((err) => {
				if (err) return done(err);
				return done();
			});
		});
		it("should 200 (OK) when deleting boat", (done) => {
			server
			.delete(`/api/boattype/${boatTypeId}`)
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
			})
			.end((err) => {
				if (err) return done(err);
				return done();
			});
		}); 
		afterAll(() => {
			server
			.delete(`/api/sport/${sportId}`)
		})
	});
});