import { randomUUID } from "crypto";
import request, { SuperAgentTest } from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Sport Router", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		//TODO: Change this URL to :3000 after finding out why it doesn't work otherwise
		server = request.agent("http://localhost:4000");
	});
	describe("Invalid JWT Requests", () => {
		it("should 401 (Unauthorized) when getting a Sport", (done) => {
			server
			.get("/api/sport/id_does_not_matter")
			.expect(401)
			.expect((res) => {
				expect(res.body.success).toBeFalsy();
				expect(res.body.error).toBe('InvalidToken');
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 401 (Unauthorized) when deleting a Sport", (done) => {
			server
			.delete("/api/sport/id_does_not_matter")
			.expect(401)
			.expect((res) => {
				expect(res.body.success).toBeFalsy();
				expect(res.body.error).toBe('InvalidToken');
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 401 (Unauthorized) when patching a Sport", (done) => {
			server
			.delete("/api/sport/id_does_not_matter")
			.send({name: "Football"})
			.expect(401)
			.expect((res) => {
				expect(res.body.success).toBeFalsy();
				expect(res.body.error).toBe('InvalidToken');
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
	});
	describe("Valid JWT Requests", () => {
		let sportId: string;
		let nonExistSportId = randomUUID();
		beforeAll(async () => {
			const res = await server
			.post("/api/login/")
			.send({ email: email, password: password });
		});
		describe("POST Requests", () => {
			describe("Bad Requests", () => {
				it("should 400 (Bad Request) when name empty", (done) => {
					server
					.post("/api/sport/")
					.send({color: "#fffff"})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'name', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when color empty", (done) => {
					server
					.post("/api/sport/")
					.send({name: "sport", color: ""})
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'color', location: 'body', value: ''}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
			});
			it("should 201 (Created) and return sport", (done) => {
				server
				.post("/api/sport/")
				.send({name: "sport", color: "#fffff"})
				.expect(201)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.id).toBeDefined();
					expect(res.body.result.name).toEqual("sport");
					expect(res.body.result.color).toEqual("#fffff");
					sportId = res.body.result.id;
					while(nonExistSportId === sportId) nonExistSportId = randomUUID();
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("GET Requests", () => {
			//TODO: Add tests for getting sport by boatId
			it("should 200 (OK) and return all sports", (done) => {
				server
				.get("/api/sport/")
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					//Since there could be multiple in this
					expect(res.body.result.length).toBeGreaterThan(0);
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("PATCH Request", () => {
			it("should 400 (Bad Request) when input empty", (done) => {
				server
				.patch(`/api/sport/${sportId}`)
				.send({name: "", color: ""})
				.expect(400)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'name', location: 'body', value: ''}, {msg: 'Invalid value', param: 'color', location: 'body', value: ''}]);
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 404 (Not Found) when patchign non existing sport", (done) => {
				server
				.patch(`/api/sport/${nonExistSportId}`)
				.send({name: "sport", color: "#fffff"})
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toEqual('sportIdNotFound');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 200 (OK) when no data", (done) => {
				server
				.patch(`/api/sport/${sportId}`)
				.send({})
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result).toEqual({});
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 200 (OK) with changed data", (done) => {
				server
				.patch(`/api/sport/${sportId}`)
				.send({name: "New Sport"})
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.name).toEqual("New Sport");
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("DELETE Request", () => {
			it("should 404 (Not Found) when deleting non existing sport", (done) => {
				server
				.delete(`/api/sport/${nonExistSportId}`)
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toEqual('sportIdDoesNotExist');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 200 (OK) when deleting sport", (done) => {
				server
				.delete(`/api/sport/${sportId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
	});
});
