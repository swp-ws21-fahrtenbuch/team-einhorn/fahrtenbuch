import { randomUUID } from "crypto";
import request, { SuperAgentTest } from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("Accounts Route", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		//TODO: Change this URL to :3000 after finding out why it doesn't work otherwise
		server = request.agent("http://localhost:4000");
	});
	
	describe("Invalid JWT Requests", () => {
		describe("GET Requests", () => {
			it("should 401 (Unauthorized) when showing all accounts", (done) => {
				server
				.get("/api/accounts/")
				.expect(401)
				.expect("Content-Type", /json/)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 401 (Unauthorized) when showing specific account", (done) => {
				server
				.get(`/api/accounts/${email}`)
				.expect(401)
				.expect("Content-Type", /json/)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("POST Request", () => {
			it("should 401 (Unauthorized) when creating account", (done) => {
				server
				.post(`/api/accounts/`)
				.send({
					first_name: "New",
					last_name: "Account",
					email: "ghost@example.de",
					role: "coordinator",
					password: "12345678"
				})
				.expect(401)
				.expect("Content-Type", /json/)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("PATCH Request", () => {
			it("should 401 (Unauthorized) when updating account", (done) => {
				server
				.patch(`/api/accounts/this-does-not-matter-anyways`)
				.send({
					first_name: "New",
					last_name: "Account",
					email: "ghost@example.de",
					role: "coordinator",
					password: "12345678"
				})
				.expect(401)
				.expect("Content-Type", /json/)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
		describe("DELETE Request", () => {
			it("should 401 (Unauthorized) when deleting account", (done) => {
				server
				.delete(`/api/accounts/this-does-not-matter-anyways`)
				.expect(401)
				.expect("Content-Type", /json/)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe('InvalidToken');
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
	});
	describe("Valid JWT Request", () => {
		let token: string;
		let accountId: string;
		let accountToBeDeleted: string;
		let nonExistUUID = randomUUID();
		//Set valid token
		beforeAll(async () => {
			const res = await server
			.post("/api/login/")
			.send({ email: email, password: password });
		});
		describe("GET Requests", () => {
			it("should 200 (OK) with result", (done) => {
				server
				.get("/api/accounts/")
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result).toBeInstanceOf(Array);
					expect(res.body.result.length).toBeGreaterThan(0);
					accountId = res.body.result[0].id;
					while(nonExistUUID === accountId) nonExistUUID = randomUUID();
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 200 (OK) with initial coordinator", (done) => {
				server
				.get(`/api/accounts/${accountId}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					let result = res.body.result;
					expect(result).toBeInstanceOf(Object);
					expect(result.id).toBe(accountId);
					expect(result.role).toBe("coordinator");
					expect(result.email).toBe(email);
					expect(result.first_name).toBe("coordinator_firstName");
					expect(result.last_name).toBe("coordinator_lastName");
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 404 (Not Found) when invalid Id is given", (done) => {
				server
				//Hacky way to generate new UUID
				.get(`/api/accounts/${nonExistUUID}`)
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toBe("accountIdNotFound");
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			})
		});
		describe("POST Request", () => {
			describe("Bad Requests", () => {
				it("should 400 (Bad Request) when no first name is given", (done) => {
					const newAccount = {
						last_name: "Account",
						email: "new@account.de",
						role: "coordinator",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'first_name', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when no last name is given", (done) => {
					const newAccount = {
						first_name: "New",
						email: "new@account.de",
						role: "coordinator",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'last_name', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when no email is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						role: "coordinator",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'email', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when invalid email is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						email: "this_is_not_an_email",
						role: "coordinator",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'email', location: 'body', value: newAccount.email}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when no role is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						email: "new@account.de",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'role', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when wrong role is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						role: "janitor",
						email: "new@account.de",
						password: "12345678"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'role', location: 'body', value: newAccount.role}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when no password is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						role: "coordinator",
						email: "new@account.de"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						//Im unsure why it sends this back two times
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'password', location: 'body'}, {msg: 'Invalid value', param: 'password', location: 'body'}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when too short password is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						role: "coordinator",
						email: "new@account.de",
						password: "1"
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'password', location: 'body', value: newAccount.password}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when password isn't string", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						role: "coordinator",
						email: "new@account.de",
						password: 1234
					}
					server
					.post("/api/accounts/")
					.send(newAccount)
					.expect(400)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([{msg: 'Invalid value', param: 'password', location: 'body', value: newAccount.password}, {msg: 'Invalid value', param: 'password', location: 'body', value: newAccount.password}]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
			});
			it("should 201 (Created) with new account data", (done) => {
				const newAccount = {
					first_name: "New",
					last_name: "Account",
					email: "new@account.de",
					role: "coordinator",
					password: "12345678"
				}
				server
				.post("/api/accounts/")
				.send(newAccount)
				.expect(201)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.account.id).toBeDefined();
					expect(res.body.account.first_name).toEqual(newAccount.first_name);
					expect(res.body.account.last_name).toEqual(newAccount.last_name);
					expect(res.body.account.email).toEqual(newAccount.email);
					expect(res.body.account.role).toEqual(newAccount.role);
					accountToBeDeleted = res.body.account.id;
					while(nonExistUUID === accountToBeDeleted || nonExistUUID === accountId) nonExistUUID = randomUUID();
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 409 (Conflict) when same email is uses again", (done) => {
				const newAccount = {
					first_name: "Another",
					last_name: "Account",
					email: "new@account.de",
					role: "boatManager",
					password: "123456789"
				}
				server
				.post("/api/accounts/")
				.send(newAccount)
				.expect(409)
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			})
		});
		describe("PATCH Requests", () => {
			describe("Bad Requests", () => {
				it("should 404 (Not Found) when wrong id is given", (done) => {
					const newAccount = {
						first_name: "New",
						last_name: "Account",
						email: "new@account.de",
						role: "coordinator",
						password: "12345678"
					}
					server
					.patch(`/api/accounts/${nonExistUUID}`)
					.send(newAccount)
					.expect(404)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.error).toEqual("accountNotFound");
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				});
				it("should 400 (Bad Request) when no data is given", (done) => {
					const newAccount = {
						first_name: "",
						last_name: "",
						email: "",
						role: "",
						password: ""
					}
					server
					.patch(`/api/accounts/${accountToBeDeleted}`)
					.expect(400)
					.send(newAccount)
					.expect((res) => {
						expect(res.body.success).toBeFalsy();
						expect(res.body.errors).toEqual([
							  {
								value: '',
								msg: 'Invalid value',
								param: 'first_name',
								location: 'body'
							  },
							  {
								value: '',
								msg: 'Invalid value',
								param: 'last_name',
								location: 'body'
							  },
							  {
								value: '',
								msg: 'Invalid value',
								param: 'email',
								location: 'body'
							  },
							  {
								value: '',
								msg: 'Invalid value',
								param: 'role',
								location: 'body'
							  },
							  {
								value: '',
								msg: 'Invalid value',
								param: 'password',
								location: 'body'
							  }
							]);
					})
					.end((err) => {
						if(err) return done(err);
						return done();
					});
				})
			});
			it("should 200 (OK) when no data at all is sent", (done) => {
				server
				.patch(`/api/accounts/${accountToBeDeleted}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result).toEqual({});
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			})
			it("should 200 (OK) when patch is applied", (done) => {
				server
				.patch(`/api/accounts/${accountToBeDeleted}`)
				.send({first_name: "John"})
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
					expect(res.body.result.id).toEqual(accountToBeDeleted);
					expect(res.body.result.first_name).toEqual("John");
					expect(res.body.result.last_name).toEqual("Account");
					expect(res.body.result.email).toEqual("new@account.de");
					expect(res.body.result.role).toEqual("coordinator");
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			})
		});
		describe("DELETE Requests", () => {
			it("should 404 (Not Found) when wrong id is given", (done) => {
				server
				.delete(`/api/accounts/${nonExistUUID}`)
				.expect(404)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.error).toEqual("accountIdDoesNotExist");
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
			it("should 200 (OK) when account is deleted", (done) => {
				server
				.delete(`/api/accounts/${accountToBeDeleted}`)
				.expect(200)
				.expect((res) => {
					expect(res.body.success).toBeTruthy();
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			});
		});
	});
});