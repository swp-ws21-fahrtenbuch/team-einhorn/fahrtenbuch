import request, { SuperAgentTest } from "supertest";
require("dotenv").config();
//All of these tests are based on routes in routes/[name].routes.ts

const email = process.env.INITIAL_COORDINATOR_EMAIL;
const password = process.env.INITIAL_COORDINATOR_PASSWORD;

describe("User Route", () => {
	let server: SuperAgentTest;
	beforeAll(() => {
		//TODO: Change this URL to :3000 after finding out why it doesn't work otherwise
		server = request.agent("http://localhost:4000");
	});
	describe("Invalid JWT Requests", () => {
		it("should 401 (Unauthorized) when no JWT is provided", (done) => {
			server
			.get("/api/user/")
			.expect(401)
			.expect((res) => {
				expect(res.body.success).toBeFalsy();
				expect(res.body.error).toBe('InvalidToken');
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 401 (Unautorized) when no JWT is provided and user tries to be patched", (done) => {
			server
			.patch(`/api/user/`)
			.send({
				first_name: "New",
				last_name: "Account",
				email: "ghost@example.de",
				role: "coordinator",
				password: "12345678"
			})
			.expect(401)
			.expect("Content-Type", /json/)
			.expect((res) => {
				expect(res.body.success).toBeFalsy();
				expect(res.body.error).toBe('InvalidToken');
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
	});
	describe("Valid JWT Requests", () => {
		beforeAll(async () => {
			const res = await server
			.post("/api/login/")
			.send({ email: email, password: password });
		});
		describe("Bad Requests", () => {
			it("should 400 (Bad Request) when no data is given", (done) => {
				const newAccount = {
					first_name: "",
					last_name: "",
					email: "",
					role: "",
					password: ""
				}
				server
				.patch(`/api/user/`)
				.expect(400)
				.send(newAccount)
				.expect((res) => {
					expect(res.body.success).toBeFalsy();
					expect(res.body.errors).toEqual([
						  {
							value: '',
							msg: 'Invalid value',
							param: 'first_name',
							location: 'body'
						  },
						  {
							value: '',
							msg: 'Invalid value',
							param: 'last_name',
							location: 'body'
						  },
						  {
							value: '',
							msg: 'Invalid value',
							param: 'email',
							location: 'body'
						  },
						  {
							value: '',
							msg: 'Invalid value',
							param: 'password',
							location: 'body'
						  }
						]);
				})
				.end((err) => {
					if(err) return done(err);
					return done();
				});
			})
		});
		it("should 200 (OK) with user", (done) => {
			server
			.get("/api/user/")
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.id).toBeDefined();
				expect(res.body.result.first_name).toEqual("coordinator_firstName")
				expect(res.body.result.last_name).toEqual("coordinator_lastName")
				expect(res.body.result.email).toEqual(email)
				expect(res.body.result.role).toEqual("coordinator")
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 200 (OK) when no data at all is sent", (done) => {
			server
			.patch(`/api/user/`)
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result).toEqual({});
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 200 (OK) when patch is applied", (done) => {
			server
			.patch(`/api/user/`)
			.send({first_name: "John"})
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.first_name).toEqual("John");
				expect(res.body.result.last_name).toEqual("coordinator_lastName");
				expect(res.body.result.email).toEqual(email);
				expect(res.body.result.role).toEqual("coordinator");
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
		it("should 200 (OK) when patch is reapplied", (done) => {
			server
			.patch(`/api/user/`)
			.send({first_name: "coordinator_firstName"})
			.expect(200)
			.expect((res) => {
				expect(res.body.success).toBeTruthy();
				expect(res.body.result.first_name).toEqual("coordinator_firstName");
				expect(res.body.result.last_name).toEqual("coordinator_lastName");
				expect(res.body.result.email).toEqual(email);
				expect(res.body.result.role).toEqual("coordinator");
			})
			.end((err) => {
				if(err) return done(err);
				return done();
			});
		});
	})
})