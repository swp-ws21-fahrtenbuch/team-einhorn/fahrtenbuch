import { Request, Response } from "express";
import BoatType from "../db/models/BoatType";
import Boat from "../db/models/Boat";
import Sport from "../db/models/Sport";

const createBoatTypeController = async (req: Request, res: Response) => {
  try {
    const { name, seats, sports } = req.body;
    const newBoatType = await BoatType.create(
      { name, seats },
    );
    let _sports: Sport[] = await Promise.all((sports.map(async (x: {id: string}) => await Sport.findByPk(x.id))))
    await newBoatType.setSports(_sports);

    return res.status(201).json({
      success: true,
      result: {
        id: newBoatType.id,
        name: newBoatType.name,
        seats: newBoatType.seats,
      },
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showAllBoatTypes = async (req: Request, res: Response) => {
  try {
    const allBoatTypes = await BoatType.findAll({
      attributes: ["id", "name", "seats"],
      include: Sport
    });
    return res.status(200).json({
      success: true,
      result: allBoatTypes,
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const deleteBoatTypeById = async (req: Request, res: Response) => {
  try {
    const boatToDelete = await BoatType.destroy({
      where: {
        id: req.params.id,
      },
    });
    if (!boatToDelete) {
      return res
        .status(404)
        .json({ success: false, error: "boatTypeIdNotFound" });
    }
    return res.status(200).json({ success: true });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showBoatTypeById = async (req: Request, res: Response) => {
  try {
    const boat = await Boat.findByPk(req.params.id);
    if (boat) {
      const boattypeid = "1"//boat.boattype;
      const boatname = await BoatType.findByPk(boattypeid);
      return res
        .status(200)
        .json({ success: true, result: { name: boatname.name } });
    }
    return res.status(404).json({ success: false, error: "boatIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const updateBoatTypeById = async (req: Request, res: Response) => {
  try {
    const input = req.body;
    //return 200 with empty response if no data was given
    if (Object.keys(input).length === 0) {
      return res
        .status(200)
        .json({ success: true, result: {}, message: "noInputFound" });
    }

    //check if boatType can be found using givenId
    if (!await BoatType.findByPk(req.params.id)) {
      return res
        .status(404)
        .json({ success: false, error: "boatTypeIdNotFound" });
    }

    //try to update
    const updatedBoatType = await BoatType.update(input, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    if(input.sports) {
      let _sports: Sport[] = await Promise.all((input.sports.map(async (x: {id: string}) => await Sport.findByPk(x.id))))
      if (!(typeof updatedBoatType[1][0] == "number")){
        await updatedBoatType[1][0].setSports(_sports);
      }
    }

    //return after updating
    const boatTypeDataAfterUpdate = updatedBoatType[1][0];
    return res.status(200).json({
      success: true,
      result: {
        id: boatTypeDataAfterUpdate.id,
        name: boatTypeDataAfterUpdate.name,
      },
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const boatTypeControllers = {
  showAllBoatTypes,
  createBoatTypeController,
  deleteBoatTypeById,
  showBoatTypeById,
  updateBoatTypeById,
};

export default boatTypeControllers;
