import { Request, Response } from "express";
import CheckIn from "../db/models/CheckIn";
import Boat from "../db/models/Boat";
import Sport from "../db/models/Sport";
import jwt from "jsonwebtoken";
import envVars from "../config";
import sendMail from "../mail";
import Employee from "../db/models/Employee";

export const checkInController = async (req: Request, res: Response) => {
  try {
    const {
      sport,
      boatName,
      startTime,
      estimatedEndTime,
      destination,
      email,
      persons,
      responsible,
    }: {
      sport: string;
      boatName: string;
      startTime: Date;
      estimatedEndTime: Date;
      destination: string;
      email: string;
      persons: string[];
      responsible: string;
    } = req.body;
    const boat = await Boat.findByPk(boatName);
    if (boat) {
      if (
        boat.status !== 0 ||
        (
          await CheckIn.findAll({
            where: { BoatId: boatName, returned: false },
          })
        ).length > 0
      ) {
        return res
          .status(400)
          .json({ success: false, error: "Boat not Availible" });
      }
    } else
      return res.status(404).json({ success: false, error: "boatIdNotFound" });

    const sportObj = await Sport.findByPk(sport);
    if (!sportObj)
      return res.status(404).json({ success: false, error: "sportIdNotFound" });

    const newLogEntry = await CheckIn.create({
      SportId: sport,
      BoatId: boatName,
      startTime,
      estimatedEndTime,
      destination,
      fullNameOfResponsibleClient: responsible,
      email,
      additionalClients: persons,
      bookingType: "default",
      returned: false,
      note: null,
      date: new Date(),
    });

    const payload = {
      id: newLogEntry.id,
    };

    const token = jwt.sign(payload, envVars.JWT_SECRET);

    if(newLogEntry.email) {
      sendMail("checkout", newLogEntry.email, "Auschecken", {
        ["application_name"]: process.env.Application_Name,
        ["name"]: newLogEntry.fullNameOfResponsibleClient,
        ["boat"]: boat.name,
        ["link"]: `${process.env.Application_URL}/checkout/${newLogEntry.id}`,
      })
    }
    //return result after checking all possible error-cases
    return res
      .cookie("checkin_token", token, {
        secure: process.env.NODE_ENV === "production",
        path: "/",
      })
      .status(201)
      .json({
        success: true,
        result: {
          id: newLogEntry.id,
          sport,
          boatName,
          startTime,
          estimatedEndTime,
          destination,
          responsible,
          email,
          persons,
          bookingType: "default",
          returned: false,
          note: null,
          date: new Date(),
        },
      });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const checkInNoteDoneController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findByPk(req.params.id);
    if(checkIn) {
      await CheckIn.update({
        noteDone: true
      },
      {
        where: {
          id: req.params.id,
        },
        returning: false
      });
      return res.status(200).json({ success: true });
    }
    return res.status(404).json({ success: false, error: "checkInNotFound" });
  }
  catch(error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
};

export const checkInNoteNotDoneController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findByPk(req.params.id);
    if(checkIn) {
      await CheckIn.update({
        noteDone: false
      },
      {
        where: {
          id: req.params.id,
        },
        returning: false
      });
      return res.status(200).json({ success: true });
    }
    return res.status(404).json({ success: false, error: "checkInNotFound" });
  }
  catch(error) {
    res.status(500).json({ success: false, error: "serverError" });
  }
};


export const checkOutController = async (req: Request, res: Response) => {
  try {
    if (!req.params.id || req.params.id == "undefined") {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }
    const checkin = await CheckIn.findByPk(req.params.id);
    if (!checkin) {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }

    const {
      note,
      bookingType,
    }: {
      note: string;
      bookingType: string;
    } = req.body;
    const updatedCheckin = await CheckIn.update(
      {
        returned: true,
        note,
      },
      {
        where: {
          id: req.params.id,
        },
        returning: true,
      }
    );
    if(updatedCheckin[1][0].note !== null) {
      let checkIn = updatedCheckin[1][0];
      let checkInBoat = await Boat.findByPk(checkIn.BoatId);
      let boatName = "undefined";
      if(checkInBoat) {
        boatName = checkInBoat.name;
      }
      let boatManagers = await Employee.findAll({
        where: {
          role: "boatManager",
        }
      });
      let date = new Date();
      boatManagers.forEach((wart:Employee) => {
        sendMail("notification", wart.email, "Bootsupdate", {
          ["application_name"]: process.env.Application_Name, 
          ["boat"]: boatName, 
          ["notification"]:checkIn.note, 
          ["author"]: checkIn.fullNameOfResponsibleClient, 
          ["timestamp"]: `${date.getDate()}.${(date.getMonth() < 10) ? '0' : ''}${date.getMonth()}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
        });
      })
    }
    return res
      .cookie("checkin_token", null, {
        secure: process.env.NODE_ENV === "production",
        path: "/",
        expires: new Date(0),
      })
      .status(200)
      .json({
        success: true,
        result: updatedCheckin,
      });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const getCheckInController = async (req: Request, res: Response) => {
  try {
    const checkin = await CheckIn.findByPk(req.params.id);
    if (checkin) {
      return res.status(200).json({ success: true, result: checkin });
    }
    return res.status(404).json({ success: false, error: "checkInIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

export const getCheckInByBoatController = async (req: Request, res: Response) => {
  try {
    const checkIn = await CheckIn.findOne({
      where: {
        BoatId: req.params.id,
        returned: false,
      }
    });
    if(checkIn) {
      return res.status(200).json({ success: true, result: checkIn });
    }
    else {
      return res.status(404).json({ success: false, error: "boatNotCheckedOut" });
    }
  }
  catch(error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
}

export const getCheckinsController = async (req: Request, res: Response) => {
  try {
    const checkIns = await CheckIn.findAll();
    return res.status(200).json({success: true, result: checkIns});
  }
  catch(error) {
    res.status(500).json({success: false, error: "serverError"});
  }
}

export const getCurrentCheckInController = async (
  req: Request,
  res: Response
) => {
  try {
    const currentId = res.locals.checkinToken.id; //payload
    const currentCheckIn = await CheckIn.findByPk(currentId);
    if (currentCheckIn) {
      return res.status(200).json({ success: true, result: currentCheckIn.id });
    } else {
      return res
        .status(404)
        .json({ success: false, error: "checkInIdNotFound" });
    }
  } catch (error) {
    res.status(500).json({ success: false, error: "serverError!" });
  }
};

export const resetCheckIns = async (req?: Request, res?: Response) => {
  const updatedCheckin = await CheckIn.update(
    {
      returned: true,
    },
    {
      where: {
        returned: false,
      },
      returning: true,
    }
  );
};
