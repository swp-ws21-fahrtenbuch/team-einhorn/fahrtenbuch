import { Request, Response } from "express";
import BoatType from "../db/models/BoatType";
import Boat from "../db/models/Boat";
import Sport from "../db/models/Sport";
import CheckIn from "../db/models/CheckIn";

const createBoat = async (req: Request, res: Response) => {
  try {
    const { name, status, boattype }: {
      name: string;
      status: number;
      boattype: string;
    } = req.body;

    //create the boat
    const newBoat = await Boat.create({
      name,
      status,
      BoatTypeId: boattype
    });

    //return created boat + the assigned sports
    if (newBoat) {
      return res.status(201).json({
        success: true,
        result: {
          id: newBoat.id,
          name: newBoat.name,
          status: newBoat.status
        },
      });
    }
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showAllBoatsController = async (req: Request, res: Response) => {
  try {
    const allBoats = await Boat.findAll();
    return res.status(200).json({
      success: true,
      result: await Promise.all(allBoats.map(async (boat) => {
        let checkins = await boat.getCheckIns({
          where: {
            returned: false,
          }
        });
        return {
          id: boat.id,
          name: boat.name,
          status: checkins[0]?.estimatedEndTime.getTime() > (new Date()).getTime() ? 2 : (checkins?.length > 0 ? 3 : boat.status),
          boattype: boat.BoatTypeId,
          // currentCheckIn: checkins?.length > 0 ? checkins[0] : null
        };
      })),
    });
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const showBoatById = async (req: Request, res: Response) => {
  try {
    const givenId = req.params.id;
    const boat = await Boat.findByPk(givenId);
    if (boat) {
      return res.status(200).json({
        success: true,
        result: {
          id: boat.id,
          name: boat.name,
          status: boat.status,
        },
      });
    }
    return res.status(404).json({ success: false, error: "boatIdNotFound" });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const deleteBoatById = async (req: Request, res: Response) => {
  try {
    const boatToDelete = await Boat.destroy({
      where: {
        id: req.params.id,
      },
    });
    if (boatToDelete == 0) {
      return res
        .status(404)
        .json({ success: false, error: "BoatIdDoesNotExist" });
    }
    return res.status(200).json({ success: true });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const updateBoatById = async (req: Request, res: Response) => {
  try {
    const { name, status, boattype }: {
      name: string;
      status: number;
      boattype: string;
    } = req.body;
    //return 200 with empty response if no data was given
    if (Object.keys(req.body).length === 0) {
      return res
        .status(200)
        .json({ success: true, result: {}, message: "noInputFound" });
    }
    //check if boat can be found using givenId
    if (!await Boat.findByPk(req.params.id)) {
      return res.status(404).json({ success: false, error: "boatIdNotFound" });
    }
    const updatedBoat = await Boat.update({
      name,
      status,
      BoatTypeId: boattype
    }, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });

    const boatDataAfterUpdate = updatedBoat[1][0];
    return res.status(200).json({
      success: true,
      result: {
        id: boatDataAfterUpdate.id,
        name: boatDataAfterUpdate.name,
        status: boatDataAfterUpdate.status,
        boattype: boatDataAfterUpdate.BoatTypeId
      },
    });
  } catch (error) {
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const getBoatOverview = async (req: Request, res: Response) => {
  try {
    const allBoats = await Boat.findAll({
      attributes: ["id", "name", "status"],
      include: [{ model: BoatType, include: [{ model: Sport }] }]
    });

    return res.status(200).json({
      success: true,
      result: await Promise.all(allBoats.map(async (boat: any) => {
        let checkins = await boat.getCheckIns({
          where: {
            returned: false,
          }
        });
        return {
          id: boat.id,
          name: boat.name,
          status: checkins[0]?.estimatedEndTime.getTime() > (new Date()).getTime() ? 2 : (checkins?.length > 0 ? 3 : boat.status),
          boattype: boat.BoatType,
          currentCheckIn: checkins?.length > 0 ? checkins[0] : null
        };
      })),
    });
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, error: "serverError" });
  }
};

const lock = async (req: Request, res: Response) => {
  if (!await Boat.findByPk(req.params.id)) {
    return res.status(404).json({ success: false, error: "boatIdNotFound" });
  }
  await Boat.update({
    status: 1,
  }, {
    where: {
      id: req.params.id,
    },
    returning: false,
  });
  return res.status(200).json({
    success: true
  });
};
const unlock = async (req: Request, res: Response) => {
  if (!await Boat.findByPk(req.params.id)) {
    return res.status(404).json({ success: false, error: "boatIdNotFound" });
  }
  await Boat.update({
    status: 0,
  }, {
    where: {
      id: req.params.id,
    },
    returning: false,
  });
  return res.status(200).json({
    success: true
  });
};


const boatControllers = {
  showAllBoatsController,
  showBoatById,
  deleteBoatById,
  createBoat,
  updateBoatById,
  getBoatOverview,
  lock,
  unlock
};

export default boatControllers;
