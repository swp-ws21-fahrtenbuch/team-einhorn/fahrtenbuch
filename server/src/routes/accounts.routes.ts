import { Router } from "express";
import { body } from "express-validator";
import accountsControllers from "../controllers/accounts.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import { validateToken } from "../middleware/validateToken";
import isCoord from "../middleware/isCoord";

const accountsRouter = Router();

//create new account
accountsRouter.post(
  "/api/accounts/",
  body("first_name").not().isEmpty(),
  body("last_name").not().isEmpty(),
  body("email").isEmail().normalizeEmail(),
  body("role").isIn(["coordinator", "boatManager"]),
  body("password").isLength({ min: 6 }).isString(),
  handleValidationResult,
  validateToken,
  isCoord,
  accountsControllers.createAccountController
);
//show all accounts
accountsRouter.get(
  "/api/accounts/",
  validateToken,
  isCoord,
  accountsControllers.showAllAccounts
);
//show specific account by id (email)
accountsRouter.get(
  "/api/accounts/:id/",
  validateToken,
  isCoord,
  accountsControllers.showAccountById
);
//update account
accountsRouter.patch(
  "/api/accounts/:id/",
  body("first_name").if(body("first_name").exists()).not().isEmpty(),
  body("last_name").if(body("last_name").exists()).not().isEmpty(),
  body("email").if(body("email").exists()).isEmail().normalizeEmail(),
  body("role").if(body("role").exists()).isIn(["coordinator", "boatManager"]),
  body("password")
    .if(body("password").exists())
    .isLength({ min: 6 })
    .isString(),
  handleValidationResult,
  validateToken,
  isCoord,
  accountsControllers.updateAccount
);
//delete account
accountsRouter.delete(
  "/api/accounts/:id/",
  validateToken,
  isCoord,
  accountsControllers.deleteAccountById
);

export default accountsRouter;
