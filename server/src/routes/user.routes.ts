import { Router } from "express";
import { validateToken } from "../middleware/validateToken";
import userControllers from "../controllers/user.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import { body } from "express-validator";

const userRouter = Router();
//show current user
userRouter.get(
  "/api/user/",
  validateToken,
  userControllers.showCurrentUserController
);

//update current user
userRouter.patch(
  "/api/user/",
  body("first_name").if(body("first_name").exists()).not().isEmpty(),
  body("last_name").if(body("last_name").exists()).not().isEmpty(),
  body("email").if(body("email").exists()).isEmail().normalizeEmail(),
  body("password").if(body("password").exists()).isLength({ min: 6 }),
  handleValidationResult,
  validateToken,
  userControllers.updateCurrentUser
);

export default userRouter;
