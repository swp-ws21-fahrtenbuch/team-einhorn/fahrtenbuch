import { Router } from "express";
import { body } from "express-validator";
import { validateToken } from "../middleware/validateToken";
import boatControllers from "../controllers/boat.controllers";
import handleValidationResult from "../middleware/handleValidationResult";
import isCoord from "../middleware/isCoord";

const boatsRouter = Router();

//show all boats
boatsRouter.get("/api/boat/", boatControllers.showAllBoatsController);
boatsRouter.get("/api/boatoverview/", boatControllers.getBoatOverview);

//show boat by given id
boatsRouter.get("/api/boat/:id", boatControllers.showBoatById);

//delete a boat
boatsRouter.delete(
  "/api/boat/:id",
  validateToken,
  boatControllers.deleteBoatById
);

//create boat
boatsRouter.post(
  "/api/boat/",
  body("name").not().isEmpty(),
  body("boattype").not().isEmpty(),
  body("status").not().isEmpty(),
  handleValidationResult,
  validateToken,
  boatControllers.createBoat
);

//update boat by id
boatsRouter.patch(
  "/api/boat/:id/",
  body("name").if(body("name").exists()).not().isEmpty(),
  body("boattype").if(body("boattype").exists()).not().isEmpty(),
  body("status").if(body("status").exists()).not().isEmpty(),
  handleValidationResult,
  validateToken,
  boatControllers.updateBoatById
);

//create boat
boatsRouter.post(
  "/api/lock/:id",
  handleValidationResult,
  validateToken,
  boatControllers.lock
);
//create boat
boatsRouter.post(
  "/api/unlock/:id",
  handleValidationResult,
  validateToken,
  boatControllers.unlock
);

export default boatsRouter;
