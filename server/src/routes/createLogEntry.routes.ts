import { Router } from "express";
import { body } from "express-validator";
import handleValidationResult from "../middleware/handleValidationResult";
import {
  checkInController,
  checkInNoteDoneController,
  checkInNoteNotDoneController,
  checkOutController,
  getCheckInByBoatController,
  getCheckInController,
  getCheckinsController,
  getCurrentCheckInController,
} from "../controllers/createLogEntry.controllers";
import {
  validateCheckinToken,
  validateToken,
} from "../middleware/validateToken";

const entryRouter = Router();

entryRouter.post(
  "/api/checkin/",
  body("startTime").isISO8601(),
  body("estimatedEndTime").isISO8601(),
  body("destination").isString(),
  body("email").isEmail().normalizeEmail(),
  body("responsible").not().isEmpty(),
  body("persons").if(body("persons").exists()).isArray(),
  handleValidationResult,
  checkInController
);
entryRouter.post("/api/checkin/noteDone/:id", validateToken, checkInNoteDoneController);
entryRouter.post("/api/checkin/noteNotDone/:id", validateToken, checkInNoteNotDoneController);
entryRouter.post("/api/checkout/:id", checkOutController);

entryRouter.get("/api/checkin/:id", getCheckInController);

entryRouter.get("/api/checkin/boat/:id", validateToken, getCheckInByBoatController);

entryRouter.get("/api/checkins/", validateToken, getCheckinsController);

entryRouter.get(
  "/api/checkin/",
  validateCheckinToken,
  getCurrentCheckInController
);

export default entryRouter;
