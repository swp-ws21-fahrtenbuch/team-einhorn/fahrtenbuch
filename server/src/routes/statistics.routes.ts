import { Router } from "express";
import statisticControllers from "../controllers/statistics.controllers";
import { validateToken } from "../middleware/validateToken";

const statisticRoute = Router();

statisticRoute.get("/api/statistic/sport/:id",validateToken, statisticControllers.showSport);
statisticRoute.get("/api/statistic/boattype/:id",validateToken, statisticControllers.showBoatType);
statisticRoute.get("/api/statistic/",validateToken, statisticControllers.showStatistic);


export default statisticRoute;
