import { NextFunction, Request, Response } from "express";

function isCoord(req: Request, res: Response, next: NextFunction) {
    if (!(res?.locals?.user?.role === "coordinator")) {
        return res.status(403)
            .json({ success: false, error: "MustBeCoordinator" });
    }
    next();
}

export default isCoord;
