import { Client } from "pg";
import { Dialect, Sequelize } from "sequelize";
import envVars from "../config";
import Boat, { initBoat } from "./models/Boat";
import BoatType, { initBoatType } from "./models/BoatType";
import CheckIn, { initCheckIn } from "./models/CheckIn";
import { initEmployee } from "./models/Employee";
import Sport, { initSport } from "./models/Sport";

export default async function initializeDatabase() {
  await createDbIfNotExists();
  const sequelize = new Sequelize(
    envVars.DB_NAME,
    envVars.DB_USER,
    envVars.DB_PASSWORD,
    {
      host: envVars.DB_HOST,
      dialect: envVars.DB_DRIVER as Dialect,
      logging: false,
    }
  );
  initCheckIn(sequelize);
  initBoatType(sequelize);
  initBoat(sequelize);
  initSport(sequelize);
  initEmployee(sequelize);

  BoatType.belongsToMany(Sport, { through: "Sport_BoatType" });
  BoatType.hasMany(Boat);
  Boat.belongsTo(BoatType);

  Sport.hasMany(CheckIn);
  CheckIn.belongsTo(Sport);

  Boat.hasMany(CheckIn);
  CheckIn.belongsTo(Boat);
  await sequelize.sync();
}

async function createDbIfNotExists() {
  const client = new Client({
    host: envVars.DB_HOST,
    user: envVars.DB_USER,
    password: envVars.DB_PASSWORD,
  });
  // https://stackoverflow.com/questions/18389124/simulate-create-database-if-not-exists-for-postgresql
  await client.connect();
  const isDatabaseExistingQuery = await client.query(
    `SELECT * FROM pg_database WHERE datname='${envVars.DB_NAME}'`
  );
  const isDatabaseExisting = isDatabaseExistingQuery.rowCount === 1;
  if (!isDatabaseExisting) {
    await client.query(`CREATE DATABASE "${envVars.DB_NAME}"`);
  }
  client.end();
}
