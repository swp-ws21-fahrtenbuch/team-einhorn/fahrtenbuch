import bcrypt from "bcrypt";
import envVars from "../config";
import Employee from "./models/Employee";

const createInitialEmployeeIfNotExists = async () => {
  const numberOfEmployees = await Employee.count();

  if (numberOfEmployees === 0) {
    const initialCoordinatorPassword = envVars.INITIAL_COORDINATOR_PASSWORD;
    const initialCoordinatorEmail = envVars.INITIAL_COORDINATOR_EMAIL;

    const hashedPassword = await bcrypt.hash(initialCoordinatorPassword, 10);

    await Employee.create({
      email: initialCoordinatorEmail,
      password: hashedPassword,
      first_name: "coordinator_firstName",
      last_name: "coordinator_lastName",
      role: "coordinator",
    });
  }
};
export default createInitialEmployeeIfNotExists;
