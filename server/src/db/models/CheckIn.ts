import { DataTypes, Model, Optional, Sequelize } from "sequelize";

interface CheckInAttributes {
  id: string;
  date: Date;
  startTime: Date;
  estimatedEndTime: Date;
  destination: string;
  email: string;
  fullNameOfResponsibleClient: string;
  additionalClients: Array<string>;
  returned: boolean;
  note: string;
  noteDone: boolean;
  bookingType: string;

  SportId?: string;
  BoatId?: string;
}
export interface CheckInAttributesInput
  extends Optional<CheckInAttributes, "id"> {}

class CheckIn
  extends Model<CheckInAttributes, CheckInAttributesInput>
  implements CheckInAttributes
{
  declare id: string;
  declare date: Date;
  declare destination: string;
  declare startTime: Date;
  declare estimatedEndTime: Date;
  declare email: string;
  declare fullNameOfResponsibleClient: string;
  declare additionalClients: Array<string>;
  declare returned: boolean;
  declare note: string;
  declare noteDone: boolean;
  declare bookingType: string;

  declare SportId?: string;
  declare BoatId?: string;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}

export const initCheckIn = (sequelizeConnection: Sequelize) => {
  CheckIn.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      destination: {
        type: DataTypes.STRING(),
        allowNull: true,
      },
      startTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      estimatedEndTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      email: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      fullNameOfResponsibleClient: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      additionalClients: {
        type: new DataTypes.ARRAY(DataTypes.STRING),
      },
      bookingType: {
        type: DataTypes.STRING(),
        allowNull: true,
      },
      returned: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      note: {
        type: DataTypes.STRING(),
        allowNull: true,
      },
      noteDone: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      }
    },
    {
      tableName: "checkin",
      sequelize: sequelizeConnection,
    }
  );
};

export default CheckIn;
