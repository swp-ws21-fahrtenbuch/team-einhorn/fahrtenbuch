import {
  Sequelize,
  DataTypes,
  Model,
  Optional,
  Association,
  HasManyAddAssociationMixin,
  BelongsToManySetAssociationsMixin,
  BelongsToManyGetAssociationsMixin
} from "sequelize";
import Sport from "./Sport";
import Boat from "./Boat";

interface BoatTypeAttributes {
  id: string;
  name: string;
  seats: number;
}
export interface BoatTypeInput extends Optional<BoatTypeAttributes, "id"> {}

class BoatType
  extends Model<BoatTypeAttributes, BoatTypeInput>
  implements BoatTypeAttributes
{
  declare id: string;
  declare name: string;
  declare seats: number;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;

  public addSport: HasManyAddAssociationMixin<Sport, any>;
  public setSports!: BelongsToManySetAssociationsMixin<Sport, number>
  public getBoats!: BelongsToManyGetAssociationsMixin<Boat>
  declare readonly sports?: Sport[];
  declare readonly boats?: Boat[];

  declare static associations: {
    sports: Association<BoatType, Sport>;
  };
}

export const initBoatType = (sequelizeConnection: Sequelize) => {
  BoatType.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.STRING(),
        allowNull: false,
      },
      seats: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      tableName: "boattype",
      sequelize: sequelizeConnection,
    }
  );
};
export default BoatType;
