import cookieParser from "cookie-parser";
import "dotenv/config";
import express from "express";
import initializeDatabase from "./db";
import createInitialEmployeeIfNotExists from "./db/createInitialEmployee";
import accountsRouter from "./routes/accounts.routes";
import authRouter from "./routes/auth.routes";
import boatsRouter from "./routes/boat.routes";
import userRouter from "./routes/user.routes";
import statisticsRouter from "./routes/statistics.routes";
import boatTypeRouter from "./routes/boatType.routes";
import entryRouter from "./routes/createLogEntry.routes";
import sportRouter from "./routes/sport.routes";
import { resetCheckIns } from "./controllers/createLogEntry.controllers"
import schedule from "node-schedule"
import path from "path";

let init = async () => {
  // DB
  console.log("Sarting Server")
  await initializeDatabase();
  await createInitialEmployeeIfNotExists();
  // server
  const app = express();
  app.use(express.json());
  app.use(cookieParser());
  app.listen(process.env.PORT || 4000, () => { });
  app.use(express.static('public'));
  app.use("/", authRouter);
  app.use("/", accountsRouter);
  app.use("/", boatsRouter);
  app.use("/", userRouter);
  app.use("/", boatTypeRouter);
  app.use("/", entryRouter);
  app.use("/", sportRouter);
  app.use("/", statisticsRouter);
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public/index.html'));
  });

  let rule = new schedule.RecurrenceRule();
  rule.tz = "Europe/Berlin";
  rule.minute = 30;
  rule.hour = 23;
  schedule.scheduleJob(rule, async (fireDate: Date) => {
    resetCheckIns();
  });
};

init();
